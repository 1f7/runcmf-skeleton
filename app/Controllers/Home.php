<?php

/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;
use RunBB\Core\Utils;

class Home
{
    private $c;

    public function __construct(Container $c)
    {
        $c['template']->loader->addPath(realpath(__DIR__.'/../Views'));
        $this->c = $c;
    }

    public function index(Request $request, Response $response, $args)
    {
        return $this->c['template']->setPageInfo([
            'title' => ['Test App', 'Index'],
            'active_page' => 'index',
            'is_indexed' => true,
        ])->display('Index');
    }

    public function about(Request $request, Response $response, $args)
    {
//        $this->c['logger']->info(__METHOD__ . ' action dispatched');

        return $this->c['template']->setPageInfo([
            'title' => ['Test App', 'About'],
            'active_page' => 'index',
            'is_indexed' => true,
        ])->display('About');
    }

    public function services(Request $request, Response $response, $args)
    {
//        $this->c['logger']->info(__METHOD__ . ' action dispatched');

        return $this->c['template']->setPageInfo([
            'title' => ['Test App', 'Services'],
            'active_page' => 'index',
            'is_indexed' => true,
        ])->display('Services');
    }
}
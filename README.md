#RunCMF Skeleton

## Create project in folder `my-app`
    $ composer create-project runcmf/runcmf-skeleton my-app  
    
## Create project in current folder
    $ composer create-project runcmf/runcmf-skeleton .  

make sure var directory writable
 
## Security

If you discover any security related issues, please email to 1f7.wizard@gmail.com or create an issue.

## Credits

* https://bitbucket.org/1f7
* https://github.com/1f7
* http://dev.runetcms.ru

## License

Apache License
Version 2.0. Please see [License File](LICENSE.md) for more information.
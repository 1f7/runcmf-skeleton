<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Start a session for flash messages
session_cache_limiter(false);
session_start();

error_reporting(-1);
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');
ini_set('log_errors', 'On');

//xdebug_start_trace(__DIR__.'/../var/log/xdebug/'.date('Ymd-His').'-trace');


require __DIR__.'/../vendor/autoload.php';

$cfg = require __DIR__ . '/../app/Config/Settings.php';

$app = new Slim\App($cfg);

// Register dependencies
require __DIR__ . '/../app/Config/Dependencies.php';

// Register middleware
require __DIR__ . '/../app/Config/Middleware.php';

// Register routes
require __DIR__ . '/../app/Config/Routes.php';

$bb = new \RunBB\Init($app);
$bb->init();

// Run app
$app->run();